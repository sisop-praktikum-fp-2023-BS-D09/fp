### Kelompok D09
| Nama | NRP |
| ------ | ------ |
| Christian Kevin Emor | 5025211153 |
| Andrian Tambunan | 5025211018 |
| Helmi Abiyu Mahendra | 5025211061 |

#### Penjelasan File Client
Kode program ini merupakan implementasi sederhana dari program client-server menggunakan protokol TCP/IP yang berfungsi untuk mengizinkan pengguna untuk berkomunikasi dengan server melalui pesan yang dikirim dan menerima respon dari server. 

##### Kode
```
int main() {
    int sock = 0;
    // file descriptor untuk socket yang akan dibuat.
    struct sockaddr_in serv_addr;
    //struct yang digunakan untuk menyimpan informasi alamat server yang akan dituju.
    char message[BUFFER_SIZE] = {0};
    //array karakter yang digunakan untuk menyimpan pesan yang akan dikirimkan ke server.
    char buffer[BUFFER_SIZE] = {0};
    // array karakter yang digunakan untuk menyimpan pesan yang diterima dari server.

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket creation failed");
        //  Jika pembuatan socket gagal, pesan kesalahan akan ditampilkan dan program akan keluar.
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    // untuk menunjukkan bahwa protokol yang digunakan adalah IPv4.
    serv_addr.sin_port = htons(PORT);//  untuk mengubah urutan byte port menjadi urutan byte jaringan yang benar. 
    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        perror("Invalid address/Address not supported");
        exit(EXIT_FAILURE);
    }
    /*
    untuk mengonversi alamat IP dalam format teks ke format biner. 
    Dalam kasus ini, alamat IP yang digunakan adalah "127.0.0.1" 
    yang merupakan loopback address (localhost).
    */
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Connection failed");
        exit(EXIT_FAILURE);
    }
    /*
    Bagian ini melakukan koneksi ke server menggunakan connect(). 
    Jika koneksi gagal, pesan kesalahan akan ditampilkan dan program 
    akan keluar.
    */
    while (1) {
        // Prompt the user to enter a message
        printf("client-service:> ");
        // Program akan menampilkan prompt "client-service:>"
        fgets(message, BUFFER_SIZE, stdin);
        message[strcspn(message, "\n")] = '\0';
        if (strcmp(message, "exit") == 0)
            break;
        // ika pengguna memasukkan pesan "exit", loop akan berhenti dan program akan keluar. 
        if (send(sock, message, strlen(message), 0) < 0) {
            perror("Send failed");
            exit(EXIT_FAILURE);
        }
        printf("Message sent to the server\n");// print
        // menampilkan pesan bahwa pesan telah berhasil dikirim ke server.
        if (read(sock, buffer, BUFFER_SIZE) < 0) {
            // membaca respon dari server menggunakan fungsi read()
            perror("Read failed");
            // menentukan jumlah maksimum karakter yang dapat dibaca. 
            // Jika pembacaan gagal, pesan kesalahan akan ditampilkan 
            // dan program akan keluar.
            exit(EXIT_FAILURE);
        }
        printf("Server response: %s\n", buffer);
        // print server response
    }

    close(sock);
    return 0;
}

```
Fungsi utama dari kodingan ini adalah sebagai berikut:

1. Membuat koneksi client-server: Program ini membuat sebuah socket client yang terhubung ke server menggunakan protokol TCP/IP. Ini dilakukan dengan membuat socket menggunakan socket(), mengatur alamat server yang akan dituju menggunakan serv_addr, dan melakukan koneksi ke server menggunakan connect().

2. Mengirim dan menerima pesan: Setelah koneksi terbentuk, program memungkinkan pengguna untuk memasukkan pesan yang akan dikirim ke server. Pesan yang dimasukkan oleh pengguna dikirim ke server menggunakan send(), dan program menunggu respon dari server menggunakan read(). Respon dari server kemudian ditampilkan di sisi client menggunakan printf().

3. Keluar dari program: Jika pengguna memasukkan pesan "exit", program akan keluar dan koneksi akan ditutup menggunakan close().



#### Penjelasan File Database

Diberikan command untuk mengakses sistem database, command yang diterima dipecah menjadi token untuk mengidentifikasi tindakan yang diperlukan
```sh
void parse_command(char *command, char *use_database){
    FILE *history;
    char path[1024];
    char text[1024];

    strcpy(path, rpath);
    strcat(path, "/history/");
    strcat(path, use_database);
    history = fopen(path, "a");

    char *token;
    token = strtok(command, ";");

    while (token != NULL){
        memset(text, 0, sizeof text);
        if (token[0] == ' '){
            memmove(token, token+1, strlen(token));
        }
        printf("%s;\n", token);
        sprintf(text, "%s;\n", token);
        fputs(text, history);
        token = strtok(NULL, ";");
    }

    fclose(history);
}
```
##### Deskripsi Problem:
###### Autentikasi
Terdapat user dan password yang digunakan untuk mengakses database yang merupakan haknya.
Program memberikan keterangan apakah user memiliki hak akses terhadap database ataupun tidak.
(user root memiliki hak akses terhadap semua database)
1)Username, password, dan hak akses database disimpan di   suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan root.
##### Potongan Code
```sh
# Checking whether client is sudo
    int sudo = 0;
    char sudo_checker[10] = {0};
    read(new_socket, sudo_checker, 1024);
    if(sudo_checker[0] == 's') sudo = 1;

# receiving username if that's not the root user
if (sudo == 0) {
        read(new_socket, username, 60);
        if(strcmp(username, "Log in failed") == 0) 
        {
            printf("%s.\n", username);
            exit(1);
        }
        else printf("Logged in with %s.\n", username);
    }
    else printf("Logged in with root.\n");

# penyimpanan username, pw, dan hak akses terhadap suatu database
void database_setup()
{
    char path[1024];
    char col[100][100] = {0};
    char type[100][100] = {0};
    int col_counter;
    char err_msg[1024] = {0};

    DIR* dir = opendir(rpath);
    if (dir) 
        closedir(dir);
    else if (ENOENT == errno) 
    {
        int flag;
        mkdir(rpath, 0777);
        
        // Creating database users
        strcpy(path, rpath);
        strcat(path, "/users");
        mkdir(path, 0777);

        // Creating table 'user_list'
        strcat(path, "/user_list");
        // Initialization
        memset(col, 0, sizeof col);
        col_counter = 2;
        strcpy(type[0], "string");
        strcpy(type[1], "string");

        // Inserting column names
        strcpy(col[0], "Username");
        strcpy(col[1], "Password");
        // Creating the table
        flag = generate_record(path, col, type, col_counter);
        if(flag)

        // Creating table 'database_access'
        strcpy(path, rpath);
        strcat(path, "/users");
        strcat(path, "/database_access");

        memset(col, 0, sizeof col);
        strcpy(col[0], "Username");
        strcpy(col[1], "Database");
        flag = generate_record(path, col, type, col_counter);

        // Creating history query directory
        stpcpy(path, rpath);
        strcat(path, "/history");
        mkdir(path, 0777);
        printf("created databases directory, history, users\n");
    } 
}

```
2)User root sudah ada dari awal.
##### Potongan Code
```sh

```

3)Menambahkan user (Hanya bisa dilakukan user root)
##### Potongan Code
```sh
if(sudo)
    {
    int check;
    check = create_user(token);
    if (check == 1) strcpy(msg, "A new user has been created.");
    else if (check == 2) strcpy(msg, "Unable to create new user (table doesn't exist).");
    else if (check == 3) strcpy(msg, "User already exist.");
    else if(check == 0) strcpy(msg, "Unable to create a new user.");
    else if(check == 11) strcpy(msg, "String data length can not be more than 50 characters.");
    else if(check == 12) strcpy(msg, "Char data length can not be more than 20 characters.");
    else if(check == 13) strcpy(msg, "Int data length can not be more than 20 characters.");
    else if(check == 14) strcpy(msg, "Int data type can not accept non numerical letter");
    }
else
    {
    strcpy(msg, "Root access is required to create a new user.");
    }

    send(new_socket, msg, strlen(msg), 0);
    continue;

int create_user(char *token)
{
    char path[1024];
    strcpy(path, rpath);
    strcat(path, "/users/user_list");
    printf("%s\n", path);

    char username[100], password[100];

    token = strtok(NULL, " ");
    if (token == NULL) return 0;

    int user_exist = check_user(token);

    if (user_exist == 0)
    {
        strcpy(username, token);

        token = strtok(NULL, " \n");
        if (token == NULL || strcmp(token, "IDENTIFIED") != 0) return 0;
        token = strtok(NULL, " \n");
        if (token == NULL || strcmp(token, "BY") != 0) return 0;
        token = strtok(NULL, " \n");
        if (token == NULL) return 0;
        else
        {
            strcpy(password, token);
            char data[100][60];
            strcpy(data[0], username);
            strcpy(data[1], password);

            int response = insert_row_user(path, data);

            if (response == 0)
            {
                return 2;
            }
            else
            {
                return response;
            }
        }
    }
    else if (user_exist == 1)
    {
        return 3;
    }
    return 0;
}
```

###### Autorisasi
1)Untuk dapat mengakses database milik user, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.
```sh
#token GRANT
# GRANT PERMISSION [nama_database] INTO [nama_user];
if (strcmp(token, "GRANT") == 0)
        {
            if(sudo == 0)
            {
                strcpy(msg, "Root access is needed to be able to grant permission.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            char tok_database[100];
            char tok_user[100];
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to grant permission.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "PERMISSION") == 0)
            {
                token = strtok(NULL, " ");
                if (token != NULL)
                {
                    strcpy(tok_database, token);
                    token = strtok(NULL, " ");
                    if (token != NULL && strcmp(token, "INTO") == 0)
                    {
                        token = strtok(NULL, " ");
                        if (token != NULL)
                        {
                            strcpy(tok_user, token);
                            int check;

                            check = check_dbaccess(tok_database, tok_user);

                            if (check == 2)
                            {
                                // masukkin row baru ke database_access
                                char path[1024];
                                char col[100][60] = {0};
                                strcpy(path, rpath);
                                strcat(path, "/users/database_access");

                                strcpy(col[0], tok_user);
                                strcpy(col[1], tok_database);

                                int insert = insert_row_user(path, col);
                                 printf("%s %s\n", col[0], col[1]);
                                if(insert == 1) {
                                    strcpy(msg, "Successfully granted a new access to the specified user.");
                                }
                                else {
                                    strcpy(msg, "Unable to grant permission.");
                                }

                                // if(insert_row(path, col, 2) == 1) strcpy(msg, "Successfully granted a new access to the specified user.");
                                // else {
                                //     printf("%s\n", path);
                                //     strcpy(msg, "Unable to grant permission.");
                                // };
                            }
                            else if (check == 1) strcpy(msg, "The user already has access to the database.");
                            else if (check == 3) strcpy(msg, "Specified user not exist.");
                            else if (check == 0) strcpy(msg, "Unable to find database.");
                        }
                        else strcpy(msg, "Unable to grant permission.");
                    }
                    else strcpy(msg, "Unable to grant permission.");
                }
                else strcpy(msg, "Unable to grant permission.");
            }
            else strcpy(msg, "Unable to grant permission.");

            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
```

2)Yang bisa memberikan permission atas database untuk suatu user hanya root.
```sh
# dari kode sebelumnya
if (strcmp(token, "GRANT") == 0)
        {
            # sudo checker
            if(sudo == 0)
            {
                strcpy(msg, "Root access is needed to be able to grant permission.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
```
3)User hanya bisa mengakses database di mana dia diberi permission untuk database tersebut.
```sh
int checker;
# dilakukan pengecekan terhadap username user untuk menentukanhah akses database
checker = check_dbaccess(token, username);
if(checker == 1 || sudo == 1) 
{
    sprintf(msg, "Database %s is now in use.", token);
    strcpy(use_database, token);
}
else if(checker == 2)
{
    sprintf(msg, "User %s cannot access this database.", username);
} 
else strcpy(msg, "Database not found.");
```

###### Data Definition Language
1)Input penamaan database, tabel, dan kolom hanya angka dan huruf.
```sh

```
2)Semua user bisa membuat database, otomatis user tersebut memiliki permission untuk database tersebut.
```sh
# pada proses pembuatan database, dilakukan pemberian nilai check akses database, yang menandai database beserta nama user pembuatnya
token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(msg, "Unable to create database.");
        send(new_socket, msg, strlen(msg), 0);
        continue;
    }
    int check;
    check = check_dbaccess(token, username);

```
3)Root dan user yang memiliki permission untuk suatu database untuk bisa membuat tabel untuk database tersebut, tentunya setelah mengakses database tersebut. Tipe data dari semua kolom adalah string atau integer. Jumlah kolom bebas.
```sh

```
4)Bisa melakukan DROP database, table (setelah mengakses database), dan kolom. Jika sasaran drop ada maka di-drop, jika tidak ada maka biarkan.
```sh
else if (strcmp(token, "DROP") == 0)
{
    printf("------drop-----\n");
    token = strtok(NULL, " ");
    if (token == NULL)
    {
        strcpy(msg, "Unable to perform drop command.");
        send(new_socket, msg, strlen(msg), 0);
        continue;
    }
    if (strcmp(token, "DATABASE") == 0)
    {
        printf("-----database-----\n");
        int checker;
        int access = 0;
        token = strtok(NULL, " ");
        if (token == NULL) {
            strcpy(msg, "Unable to drop database.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        // Checking access & checking whether database exist
        if (sudo) access = 1;
        else {
            checker = check_dbaccess(token, username);
            access = checker;
        }
        if (access == 1)
        {
            char database_path[200];
            strcpy(database_path, rpath);
            strcat(database_path, "/");
            strcat(database_path, token);
            char drop_database[1024];
            memset(drop_database, 0, sizeof drop_database);
            sprintf(drop_database, "%s %s", "rm -rf", database_path);
            system(drop_database);
            if(strcmp(use_database, token) == 0) strcpy(use_database, "");
            strcpy(msg, "Successfully dropped database.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        else if (access = 2){
            sprintf(msg, "User %s unable to drop this database.", username);
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        else if (access = 0){
            strcpy(msg, "Database not exist.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
    }
    else if (strcmp(token, "TABLE") == 0)
    {
        printf("-----table-----\n");
        int checker = 0;
        token = strtok(NULL, " ");
        if (token == NULL) 
        {
            strcpy(msg, "Unable to drop database.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        if(strcmp(use_database, "") == 0) 
        {
            strcpy(msg, "No database is used.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        char table_path[200];
        strcpy(table_path, rpath);
        strcat(table_path, "/");
        strcat(table_path, use_database);
        strcat(table_path, "/");
        strcat(table_path, token);
        checker = find_table(table_path);
        if (checker == 51)
        {
            drop_table(table_path);
            strcpy(msg, "Successfully dropped the table.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        else
        {
            strcpy(msg, "No table found.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
    }   
    else if (strcmp(token, "COLUMN") == 0)
    {
        int checker;
        char target_col[60];
        token = strtok(NULL, " ");
        if (token == NULL)
        {
            strcpy(msg, "Unable to drop column.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        strcpy(target_col, token);
        token = strtok(NULL, " ");
        if (token == NULL || strcmp(token, "FROM") != 0)
        {
            strcpy(msg, "Unable to drop column.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
        token = strtok(NULL, " ");
        char table_path[200];
        strcpy(table_path, rpath);
        strcat(table_path, "/");
        strcat(table_path, use_database);
        strcat(table_path, "/");
        strcat(table_path, token);
        // Checking the existance of the table
        checker = find_table(table_path);
        if (checker == 51) // table found
        {
            checker = drop_column(table_path, token, target_col);
            if (checker == 0) strcpy(msg, "No column found.");
            else if (checker == 1) strcpy(msg, "Column successfully deleted.");
            send(new_socket, msg, strlen(msg), 0);
        }
        else
        {
            strcpy(msg, "No table found.");
            send(new_socket, msg, strlen(msg), 0);
            continue;
        }
    }
}

```

##### Penjelasan


###### Data Modification Language
1)INSERT
Hanya bisa insert satu row per satu command. Insert sesuai dengan jumlah dan urutan kolom.
```sh
else if (strcmp(token, "INSERT") == 0)
        {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to insert column.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "INTO") == 0)
            {
                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to insert column.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else 
                {
                    char table_path[200];
                    strcpy(table_path, rpath);
                    strcat(table_path, "/");
                    strcat(table_path, use_database);
                    strcat(table_path, "/");
                    strcat(table_path, token);

                    // Checking whether table exist in the database or not
                    int checker;
                    checker = find_table(table_path);
                    if (checker == 51)
                    {
                        // counting column in the specified table
                        int numOfCol;
                        numOfCol = count_column(table_path);

                        // parse values
                        if (token == NULL) 
                        {
                            strcpy(msg, "Unable to insert column.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }

                        char data[100][60];
                        memset(data, 0, sizeof data);
                        for(int i = 0; i < numOfCol; i++)
                        {
                            int dataChar = 0;
                            token = strtok(NULL, ",");
                            if(i != 0 && token == NULL) // kekurangan data
                            {
                                strcpy(msg, "Insufficient argument. Unable to perform insert.");
                                send(new_socket, msg, strlen(msg), 0);
                                continue;
                            }
                            int len = strlen(token);
                            for(int j = 0; j < len; j++)
                            {
                                if( j == 0 && ( token[j] == '(' || token[j] == ' ' ) ||
                                    j == len - 1 && token[j] == ')')
                                {
                                    continue;
                                }
                                else if((token[j] < 48 || token[j] > 57) && j != 0 && j != len-1 &&
                                        ( !(token[1] == '\'' && token[len - 1] == '\'') && 
                                         !(token[1] == '\'' && token[len - 2] == '\'' && token[len - 1] == ')'))) // alfabet tanpa tanda petik
                                {
                                    strcpy(msg, "String or char data type need ' in between.");
                                    send(new_socket, msg, strlen(msg), 0);
                                    continue;
                                }
                                else 
                                {
                                    data[i][dataChar] = token[j];
                                    dataChar++;
                                }
                            }
                            printf("%s\n", data[i]);
                        }
                        token = strtok(NULL, ",");
                        if (token != NULL) // kelebihan data
                        {
                            strcpy(msg, "Excessive argument. Unable to perform insert.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }

                        int check = insert_row(table_path, data);

                        if (check == 1) strcpy(msg, "A new record has been inserted.");
                        else if(check == 11) strcpy(msg, "String data length can not be more than 50 characters.");
                        else if(check == 12) strcpy(msg, "Char data length can not be more than 20 characters.");
                        else if(check == 13) strcpy(msg, "Int data length can not be more than 20 characters.");
                        else if(check == 14) strcpy(msg, "Int data type can not accept non numerical letter.");
                        else if(check == 15) strcpy(msg, "Input of char data type need ' in between.");
                        else if(check == 16) strcpy(msg, "Input of string data type need ' in between.");

                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                            
                    }
                    else
                    {
                        strcpy(msg, "No table found.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }

                }
            }
        }
```
2)UPDATE
Hanya bisa update satu kolom per satu command.
```sh
kosong pas demo
```
3)DELETE
Delete data yang ada di tabel.
```sh
else if (strcmp(token, "DELETE") == 0) {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to perform select.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "FROM") == 0)
            {
                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to perform select.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                else {
                    char table_path[200];
                    strcpy(table_path, rpath);
                    strcat(table_path, "/");
                    strcat(table_path, use_database);
                    strcat(table_path, "/");
                    strcat(table_path, token);
                    int check_table = find_table(table_path);
                    if(check_table == 51) {
                            # deleting table
                        if(delete_table(table_path) == 1) {
                            strcpy(msg, "Table content has been deleted.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }
                        else {
                            strcpy(msg, "Deleting table content failed.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }
                    }
                }
            }
        }
```
4)SELECT
```sh
else if (strcmp(token, "SELECT") == 0) {
            token = strtok(NULL, " ");
            if (token == NULL) 
            {
                strcpy(msg, "Unable to perform select.");
                send(new_socket, msg, strlen(msg), 0);
                continue;
            }
            else if (strcmp(token, "*") == 0)
            {
                token = strtok(NULL, " ");
                if (token == NULL) 
                {
                    strcpy(msg, "Unable to perform select.");
                    send(new_socket, msg, strlen(msg), 0);
                    continue;
                }
                # jika asal file tidak didefinisikan
                else if(strcmp(token, "FROM") == 0) {
                    token = strtok(NULL, " ");
                    if (token == NULL) 
                    {
                        strcpy(msg, "Unable to perform select.");
                        send(new_socket, msg, strlen(msg), 0);
                        continue;
                    }
                    else {
                        char table_path[200];
                        strcpy(table_path, rpath);
                        strcat(table_path, "/");
                        strcat(table_path, use_database);
                        strcat(table_path, "/");
                        strcat(table_path, token);

                        int check_table = find_table(table_path);

                        if(check_table == 51) {
                            print_all(table_path);
                            strcpy(msg, "Table content has been printed.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                        }
                       else {
                            strcpy(msg, "Table not found.");
                            send(new_socket, msg, strlen(msg), 0);
                            continue;
                       }
                    }
                }
            }
            else {
                //kosong
            }
        }
```
5)WHERE
Command UPDATE, SELECT, dan DELETE bisa dikombinasikan dengan WHERE. WHERE hanya untuk satu kondisi. Dan hanya ‘=’.

```sh
hello world
```

###### Logging
Setiap command yang dipakai harus dilakukan logging ke suatu file dengan format. Jika yang eksekusi root, maka username root.
##### Solusi

##### Potongan Code
```sh
hello world
```
##### Penjelasan

###### Reliability
1)Harus membuat suatu program terpisah untuk dump database ke command-command yang akan di print ke layar. Untuk memasukkan ke file, gunakan redirection. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai database level saja, tidak perlu sampai tabel.
2)Program dump database dijalankan tiap jam untuk semua database dan log, lalu di zip sesuai timestamp, lalu log dikosongkan kembali.
##### Solusi

##### Potongan Code
```sh
hello world
```
##### Penjelasan

#### Penjelasan Code Client_Dump
###### Library
````
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
````
Untuk mengkases library yang akan digunakan selama proses berlangsung

###### Main
````
int main(int argc, char const *argv[])
{
    // memvalidasi argumen yang diberikan saat menjalankan program
    if (!validateArguments(argc, argv))
    {
        printf("Error: Invalid arguments.\n");
        return -1;
    }
    //  menyimpan informasi alamat server
    struct sockaddr_in serverAddress;

    // buat soket dan melakukan koneksi ke server
    int socketDescriptor = createSocketAndConnect(&serverAddress);

    // memeriksa apakah koneksi ke server berhasil atau tidak
    if (socketDescriptor < 0)
    {
        printf("Error: Connection failed.\n");
        return -1;
    }

    char type[1024];
    //  memeriksa apakah program dijalankan oleh pengguna root atau bukan
    if (getuid())
    {
        sprintf(type, "%s %s dump", argv[2], argv[4]);
    }
    else
    {
        strcpy(type, "root dump");
    }

    // untuk mengirimkan data (jenis operasi) melalui soket
    sendData(socketDescriptor, type);

    // menyimpan status hasil autentikasi
    char statusLogin[1000];
    if (!receiveData(socketDescriptor, statusLogin, sizeof(statusLogin)))
    {
        close(socketDescriptor);
        printf("Error: Authentication failed.\n");
        return -1;
    }

    //  menyimpan perintah yang akan dikirim melalui soket
    char command[1000];
    sprintf(command, "USE %s", getuid() ? argv[5] : argv[1]);

    //  mengirimkan perintah "USE [nama_database]" melalui soket
    sendData(socketDescriptor, command);

    //  menyimpan data yang diterima melalui soket
    char receive[1000];
    strcpy(command, "continue");
    sendData(socketDescriptor, command);

    // memeriksa apakah data yang diterima melalui soket memuat string "Database changed to" di awalnya
    if (strncmp(receive, "Database changed to", 19) == 0)
    {
        handleDatabaseChange(socketDescriptor, argv[5]);
        close(socketDescriptor);
        return 0;
    }
    close(socketDescriptor);
    return 0;
}
````
Fungsi utama dalam menjalankan program client_dump

###### validateArguments
````
bool validateArguments(int argc, char const *argv[])
{
    // memeriksa apakah pengguna saat ini bukan root user
    if (getuid())
    {
        //memeriksa apakah jumlah argumen yang diberikan
        if (argc < 6 || (strcmp(argv[1], "-u") != 0) || (strcmp(argv[3], "-p") != 0))
        {
            return false;
        }
    }
    else
    {
        //memeriksa apakah jumlah argumen yang diberikan kurang dari 2
        if (argc < 2)
        {
            return false;
        }
    }
    return true;
}
````
fungsi untuk memvalidasi argumen

###### createSocketAndConnect
````
int createSocketAndConnect(struct sockaddr_in *serverAddress)
{
    int socketDescriptor;

    // buat soket dengan memanggil fungsi socket
    if ((socketDescriptor = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        return -1;
    }

    // mengatur semua byte dari variabel serverAddress menjadi nol
    memset(serverAddress, '0', sizeof(*serverAddress));

    // menandakan penggunaan alamat IPv4
    serverAddress->sin_family = AF_INET;

    // mengonversi nomor port ke urutan byte jaringan
    serverAddress->sin_port = htons(PORT);

    //  mengonversi alamat IPv4 "127.0.0.1" ke bentuk biner
    if (inet_pton(AF_INET, "127.0.0.1", &(serverAddress->sin_addr)) <= 0)
    {
        return -1;
    }

    // kondisi yang mencoba melakukan koneksi ke server
    if (connect(socketDescriptor, (struct sockaddr *)serverAddress, sizeof(*serverAddress)) < 0)
    {
        return -1;
    }
    return socketDescriptor;
}
````
buat soket dan melakukan koneksi ke server

###### sendData
````
void sendData(int socketDescriptor, const char *data)
{
    // data akan dikirimkan melalui soket sesuai dengan deskriptor soket yang telah ditentukan
    send(socketDescriptor, data, strlen(data), 0);
}

// menerima data dari soket ke buffer yang ditentukan
bool receiveData(int socketDescriptor, char *buffer, int bufferSize)
{
    // digunakan untuk menerima data dari soket
    int readValue = recv(socketDescriptor, buffer, bufferSize, 0);
    if (readValue == 0)
    {
        // menutup soket jika penerimaan data berhasil dilakukan
        close(socketDescriptor);
        return false;
    }
    return true;
}
````
Digunakan untuk mengirimkan data

###### handleDatabaseChange
````
void handleDatabaseChange(int socketDescriptor, const char *databaseName)
{
    char receive[1000];
    // mengelola perubahan database dengan menerima dan memproses pesan dari soket
    do
    {
        // mengatur semua byte dalam receive menjadi nol sebelum menerima data baru dari soket
        memset(receive, 0, sizeof(receive));

        // menerima data dari soket dan menyimpannya
        receiveData(socketDescriptor, receive, sizeof(receive));

        //  memeriksa apakah pesan yang diterima bukan "Done"
        if (strcmp(receive, "Done") != 0)
        {
            printf("%s\n", receive);
            char command[1000];
            strcpy(command, "continue");

            // mengirimkan pesan "continue" ke soket
            sendData(socketDescriptor, command);
        }
    } while (strcmp(receive, "Done") != 0);
}
````
mengelola perubahan database pada soket yang ditentukan

###### isSocketClosed
````
bool isSocketClosed(int readValue, int socketDescriptor)
{
    // memeriksa byte yang berhasil dibaca dari soket
    if (readValue == 0)
    {
        close(socketDescriptor);
        return true;
    }
    return false;
}
````
memeriksa apakah soket telah ditutup oleh pihak lain
