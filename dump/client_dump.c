#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>

//  nomor port yang akan digunakan saat membuat koneksi socket
#define PORT 9090

// memeriksa apakah soket telah ditutup atau tidak
bool isSocketClosed(int readValue, int socketDescriptor);

// digunakan untuk memvalidasi argumen yang diberikan saat menjalankan program
bool validateArguments(int argc, char const *argv[]);

// membuat soket dan menghubungkannya ke server
int createSocketAndConnect(struct sockaddr_in *serverAddress);

// mengirim data melalui soket
void sendData(int socketDescriptor, const char *data);

// menerima data dari soket
bool receiveData(int socketDescriptor, char *buffer, int bufferSize);

// bertanggung jawab untuk menangani perubahan database dengan mengirimkan nama database melalui soket
void handleDatabaseChange(int socketDescriptor, const char *databaseName);

int main(int argc, char const *argv[])
{
    // memvalidasi argumen yang diberikan saat menjalankan program
    if (!validateArguments(argc, argv))
    {
        printf("Error: Invalid arguments.\n");
        return -1;
    }
    //  menyimpan informasi alamat server
    struct sockaddr_in serverAddress;

    // buat soket dan melakukan koneksi ke server
    int socketDescriptor = createSocketAndConnect(&serverAddress);

    // memeriksa apakah koneksi ke server berhasil atau tidak
    if (socketDescriptor < 0)
    {
        printf("Error: Connection failed.\n");
        return -1;
    }

    char type[1024];
    //  memeriksa apakah program dijalankan oleh pengguna root atau bukan
    if (getuid())
    {
        sprintf(type, "%s %s dump", argv[2], argv[4]);
    }
    else
    {
        strcpy(type, "root dump");
    }

    // untuk mengirimkan data (jenis operasi) melalui soket
    sendData(socketDescriptor, type);

    // menyimpan status hasil autentikasi
    char statusLogin[1000];
    if (!receiveData(socketDescriptor, statusLogin, sizeof(statusLogin)))
    {
        close(socketDescriptor);
        printf("Error: Authentication failed.\n");
        return -1;
    }

    //  menyimpan perintah yang akan dikirim melalui soket
    char command[1000];
    sprintf(command, "USE %s", getuid() ? argv[5] : argv[1]);

    //  mengirimkan perintah "USE [nama_database]" melalui soket
    sendData(socketDescriptor, command);

    //  menyimpan data yang diterima melalui soket
    char receive[1000];
    strcpy(command, "continue");
    sendData(socketDescriptor, command);

    // memeriksa apakah data yang diterima melalui soket memuat string "Database changed to" di awalnya
    if (strncmp(receive, "Database changed to", 19) == 0)
    {
        handleDatabaseChange(socketDescriptor, argv[5]);
        close(socketDescriptor);
        return 0;
    }
    close(socketDescriptor);
    return 0;
}

//memvalidasi argumen yang diberikan ke program
bool validateArguments(int argc, char const *argv[])
{
    // memeriksa apakah pengguna saat ini bukan root user
    if (getuid())
    {
        //memeriksa apakah jumlah argumen yang diberikan
        if (argc < 6 || (strcmp(argv[1], "-u") != 0) || (strcmp(argv[3], "-p") != 0))
        {
            return false;
        }
    }
    else
    {
        //memeriksa apakah jumlah argumen yang diberikan kurang dari 2
        if (argc < 2)
        {
            return false;
        }
    }
    return true;
}

// buat soket dan melakukan koneksi ke server
int createSocketAndConnect(struct sockaddr_in *serverAddress)
{
    int socketDescriptor;

    // buat soket dengan memanggil fungsi socket
    if ((socketDescriptor = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        return -1;
    }

    // mengatur semua byte dari variabel serverAddress menjadi nol
    memset(serverAddress, '0', sizeof(*serverAddress));

    // menandakan penggunaan alamat IPv4
    serverAddress->sin_family = AF_INET;

    // mengonversi nomor port ke urutan byte jaringan
    serverAddress->sin_port = htons(PORT);

    //  mengonversi alamat IPv4 "127.0.0.1" ke bentuk biner
    if (inet_pton(AF_INET, "127.0.0.1", &(serverAddress->sin_addr)) <= 0)
    {
        return -1;
    }

    // kondisi yang mencoba melakukan koneksi ke server
    if (connect(socketDescriptor, (struct sockaddr *)serverAddress, sizeof(*serverAddress)) < 0)
    {
        return -1;
    }
    return socketDescriptor;
}

// digunakan untuk mengirim data
void sendData(int socketDescriptor, const char *data)
{
    // data akan dikirimkan melalui soket sesuai dengan deskriptor soket yang telah ditentukan
    send(socketDescriptor, data, strlen(data), 0);
}

// menerima data dari soket ke buffer yang ditentukan
bool receiveData(int socketDescriptor, char *buffer, int bufferSize)
{
    // digunakan untuk menerima data dari soket
    int readValue = recv(socketDescriptor, buffer, bufferSize, 0);
    if (readValue == 0)
    {
        // menutup soket jika penerimaan data berhasil dilakukan
        close(socketDescriptor);
        return false;
    }
    return true;
}

//  mengelola perubahan database pada soket yang ditentukan
void handleDatabaseChange(int socketDescriptor, const char *databaseName)
{
    char receive[1000];
    // mengelola perubahan database dengan menerima dan memproses pesan dari soket
    do
    {
        // mengatur semua byte dalam receive menjadi nol sebelum menerima data baru dari soket
        memset(receive, 0, sizeof(receive));

        // menerima data dari soket dan menyimpannya
        receiveData(socketDescriptor, receive, sizeof(receive));

        //  memeriksa apakah pesan yang diterima bukan "Done"
        if (strcmp(receive, "Done") != 0)
        {
            printf("%s\n", receive);
            char command[1000];
            strcpy(command, "continue");

            // mengirimkan pesan "continue" ke soket
            sendData(socketDescriptor, command);
        }
    } while (strcmp(receive, "Done") != 0);
}

//  memeriksa apakah soket telah ditutup oleh pihak lain
bool isSocketClosed(int readValue, int socketDescriptor)
{
    // memeriksa byte yang berhasil dibaca dari soket
    if (readValue == 0)
    {
        close(socketDescriptor);
        return true;
    }
    return false;
}
