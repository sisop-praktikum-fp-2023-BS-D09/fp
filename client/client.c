#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 1024
// ukuran buffer yang digunakan untuk mengirim dan menerima data.

int main() {
    int sock = 0;
    // file descriptor untuk socket yang akan dibuat.
    struct sockaddr_in serv_addr;
    //struct yang digunakan untuk menyimpan informasi alamat server yang akan dituju.
    char message[BUFFER_SIZE] = {0};
    //array karakter yang digunakan untuk menyimpan pesan yang akan dikirimkan ke server.
    char buffer[BUFFER_SIZE] = {0};
    // array karakter yang digunakan untuk menyimpan pesan yang diterima dari server.

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket creation failed");
        //  Jika pembuatan socket gagal, pesan kesalahan akan ditampilkan dan program akan keluar.
        exit(EXIT_FAILURE);
    }

    serv_addr.sin_family = AF_INET;
    // untuk menunjukkan bahwa protokol yang digunakan adalah IPv4.
    serv_addr.sin_port = htons(PORT);//  untuk mengubah urutan byte port menjadi urutan byte jaringan yang benar. 
    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        perror("Invalid address/Address not supported");
        exit(EXIT_FAILURE);
    }
    /*
    untuk mengonversi alamat IP dalam format teks ke format biner. 
    Dalam kasus ini, alamat IP yang digunakan adalah "127.0.0.1" 
    yang merupakan loopback address (localhost).
    */
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Connection failed");
        exit(EXIT_FAILURE);
    }
    /*
    Bagian ini melakukan koneksi ke server menggunakan connect(). 
    Jika koneksi gagal, pesan kesalahan akan ditampilkan dan program 
    akan keluar.
    */
    while (1) {
        // Prompt the user to enter a message
        printf("client-service:> ");
        // Program akan menampilkan prompt "client-service:>"
        fgets(message, BUFFER_SIZE, stdin);
        message[strcspn(message, "\n")] = '\0';
        if (strcmp(message, "exit") == 0)
            break;
        // ika pengguna memasukkan pesan "exit", loop akan berhenti dan program akan keluar. 
        if (send(sock, message, strlen(message), 0) < 0) {
            perror("Send failed");
            exit(EXIT_FAILURE);
        }
        printf("Message sent to the server\n");// print
        // menampilkan pesan bahwa pesan telah berhasil dikirim ke server.
        if (read(sock, buffer, BUFFER_SIZE) < 0) {
            // membaca respon dari server menggunakan fungsi read()
            perror("Read failed");
            // menentukan jumlah maksimum karakter yang dapat dibaca. 
            // Jika pembacaan gagal, pesan kesalahan akan ditampilkan 
            // dan program akan keluar.
            exit(EXIT_FAILURE);
        }
        printf("Server response: %s\n", buffer);
        // print server response
    }

    close(sock);
    // Pada akhir program, socket ditutup menggunakan close() 
    // untuk membebaskan sumber daya yang digunakan oleh socket tersebut.

    return 0;
}

